<?php

namespace App\Http\Controllers;

use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Tests\Controllers;

class logoController extends Controller
{
    /* === Index Function === */
    public function index() {
        $logo = DB::select('select * from logo');
        return view('admin/logo', compact('logo'));
    }

    /* === Insert Function === */
    public function insert(Request $request) {
        $image = $request->file('logo_image');
        $name = strtolower(str_replace(' ','',$request->file('logo_image')->getClientOriginalName()));
        $destinationPath = public_path('/upload/images/');
        $image->move($destinationPath, $name);
        DB::table('logo')
        	->insert([
            	'logo' => $name
        	]);
        return redirect('logo');
    }

    /* === Update Function === */
    public function get($id) {
        $data['logo'] = $id;
        $logo = DB::select('select * from logo WHERE logo = ?', [$data['logo']]);
        return view('admin/update_logo', compact('logo'));
        $image_path = public_path().'/upload/images/'.$data['logo']; 
        unlink($image_path);
    }
    public function update(Request $request) {
        print_r(logo);
        exit;
        $image = $request->file('logo_image');
        $name = strtolower(str_replace(' ','',$request->file('logo_image')->getClientOriginalName()));
        $destinationPath = public_path('/upload/images/');
        //File::delete($destinationPath, $logo); // Delete old flyer
        $image->move($destinationPath, $name);
        DB::table('logo')
            ->update([
            	'logo' => $name
            ]);
        return redirect('logo');
    }

    /* === Delete Function === */
    public function delete($id) {
    	$data['logo'] = $id;
        DB::delete('delete from logo WHERE logo = ?', [$data['logo']]);
        $image_path = public_path().'/upload/images/'.$data['logo']; 
        unlink($image_path);
        return redirect('logo');
    }
}
