<?php

namespace App\Http\Controllers;

use DB;
use Symfony\Component\HttpKernel\Tests\Controllers;

class getDatabase extends Controller
{
    function index() {
        $navigation = DB::select('select * from navigation');
        return view('index', compact('navigation'));
    }

    function insert() {
    	DB::insert('insert into navigation (title) values (?)', [$_POST['facebook']]);
        return view('admin/sosmed');
    }
}
