<?php

namespace App\Http\Controllers;

use DB;
use Symfony\Component\HttpKernel\Tests\Controllers;

class sosmedController extends Controller
{
    /* === Index Function === */
    public function index() {
        $sosmed = DB::select('select * from sosmed');
        return view('admin/sosmed', compact('sosmed'));
    }

    /* === Insert Function === */
    public function insert() {
        DB::table('sosmed')
        	->insert([
            	'facebook' => $_POST['facebook'], 
            	'twitter' => $_POST['twitter'], 
            	'youtube' => $_POST['youtube'], 
            	'instagram' => $_POST['instagram']
        	]);
        return redirect('sosmed');
    }

    /* === Update Function === */
    public function get($id) {
        $data['id_sosmed'] = $id;
        $sosmed = DB::select('select * from sosmed WHERE id_sosmed = ?', [$data['id_sosmed']]);
        return view('admin/update_sosmed', compact('sosmed'));
    }
    public function update() {
        DB::table('sosmed')
            ->where('id_sosmed', $_POST['id_sosmed'])
            ->update([
            	'facebook' => $_POST['facebook'], 
            	'twitter' => $_POST['twitter'],
            	'youtube' => $_POST['youtube'],
            	'instagram' => $_POST['instagram']
            ]);
        return redirect('admin/sosmed');
    }

    /* === Delete Function === */
    public function delete($id) {
    	$data['id_sosmed'] = $id;
        DB::delete('delete from sosmed WHERE id_sosmed = ?', [$data['id_sosmed']]);
        return redirect('sosmed');
    }
}
