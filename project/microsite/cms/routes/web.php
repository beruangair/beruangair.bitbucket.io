<?php

use App\Http\Middleware\CheckAge;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/foo', function () {
    return redirect()->route('profile');
});

Route::get('user/profile', function () {
    //
})->name('profile');

Route::get('/home', function() {
	return 'Your age is less than 100';
});

Route::get('/user/age/{age}', function() {
	return 'You have permission';
})->middleware(CheckAge::class);

/*Route::get('/showRedirect', function() {
	return 'Hi redirect here';
})->name('profile');

Route::get('user/{id}', function($id) {
	return '<h1>User'.$id.'</h1>';
});*/

Route::get('/post/{post}/coments/{coments}', function($postId, $comentsId) {
	echo "Post".$postId;
	echo "<br>";
	echo "Coments".$comentsId;
});

Route::get('user/{name?}', function ($name = 'John') {
    return $name;
});


/*Route::resource('index', 'indexController');*/

/*
Route::get('index', function index() {
    return 'Hello World';
});*/

// ------ Index ------- //

// Index Page
Route::get('/index', 'indexController@index');



// ------ Admin ------- //

// Dashboard
Route::resource('admin', 'adminController');



/* === Logo Routes === */

/* - Index - */
Route::get('/logo', 'logoController@index');

/* - Insert - */
Route::get('/logo/insert', function ()  {
	return view ('admin/input_logo');
});
Route::post('insertLogo', 'logoController@insert');

/* - Get - */
Route::get('/get/{id_logo}', ['uses' => 'logoController@get']);

/* - Update - */
Route::post('/updateLogo', ['uses' => 'logoController@update']);

/* - Delete - */
Route::get('/delete/{id_logo}', ['uses' => 'logoController@delete']);



/* === Sosmed Routes === */

// Sosmed
Route::get('/sosmed', 'sosmedController@index');

// Insert
Route::get('insert', function ()  {
	return view ('input_sosmed');
});
Route::post('sosmed', 'sosmedController@insert');

// Update
Route::post('/update', ['uses' => 'sosmedController@update']);

// Get
Route::get('/get/{id_sosmed}', ['uses' => 'sosmedController@get']);

// Delete
Route::get('/delete/{id_sosmed}', ['uses' => 'sosmedController@delete']);
