<!DOCTYPE html>
<html lang="en">
<head>
	<title>DPR RI</title>
	@include('admin/components/meta') <!-- Meta -->
	@include('admin/components/icons') <!-- SEO Icon -->
	@include('admin/components/meta-facebook') <!-- Meta Facebook -->
	@include('admin/components/meta-twitter') <!-- Meta Twitter -->
	@include('admin/components/seo-crawling') <!-- SEO Crawling -->
	@include('admin/components/dns') <!-- DNS + Browser Rendering -->
	
	<!-- Start CSS Link From Here -->
	<link rel="stylesheet" href="{{ URL::asset('css/styles.css') }}"> <!-- Styles -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<!-- Start CSS Link End Here -->
</head>
<body>
	<header>
		<div class="container clearfix">
			<!-- Navigation -->
			<nav class="pull-left">
				<ul>
					@for ($i = 0; $i < count($navigation); $i++)
						<li><a href="#">{{ $navigation[$i]->title }}</a></li>
					@endfor
				</ul>
			</nav>
			<!-- Social Media Header -->
			<div class="sosmed-header pull-right">
				<ul>
					<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="header-mobile clearfix">
			<div class="logo-mobile pull-left">
				<img src="{{ URL::asset('images/logo.png') }}" alt="Dewan Perwakilan Rakyat Republik Indonesia">
			</div>
			<div class="navbar pull-right">
				<button type="button" class="navbar-toggle">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
			</div>
		</div>
		<div class="menu-mobile">
			<!-- Navigation -->
			<nav>
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">TVR Parlemen</a></li>
					<li><a href="#">Berita DPR RI</a></li>
					<li><a href="#">Official DPR RI Website</a></li>
				</ul>
			</nav>
			<!-- Social Media Header -->
			<div class="sosmed-header">
				<ul>
					<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</header>
	<main>
		<div class="container">
			<section class="logo"><a href="#"><img src="{{ URL::asset('images/logo.png') }}" alt="Dewan Perwakilan Rakyat Republik Indonesia"></a></section>
			<section class="pimpinan">
				<div class="title">Pimpinan DPR RI</div>
				<div class="picture">
					<ul>
						<li><a href="#"><img src="{{ URL::asset('images/taufik.jpg') }}" alt=""></a></li>
						<li><a href="#"><img src="{{ URL::asset('images/fadlizon.jpg') }}" alt=""></a></li>
						<li><a href="#"><img src="{{ URL::asset('images/setnov.jpg') }}" alt=""></a></li>
						<li><a href="#"><img src="{{ URL::asset('images/agus.jpg') }}" alt=""></a></li>
						<li><a href="#"><img src="{{ URL::asset('images/fahri.jpg') }}" alt=""></a></li>
					</ul>
				</div>
			</section>
			<div class="block clearfix">
				<section class="berita pull-left">
					<div class="list-news big clearfix">
						<div class="col-md-4">
							<div class="title">Berita Dari DPR</div>
							<a href="#"><img src="{{ URL::asset('images/news-1.jpg') }}" class="lazy img-responsive" alt=""></a>
						</div>
						<div class="col-md-8">
							<div class="field-news">
								<a href="#" class="caption">Penjelasan Sekjen DPR RI Terkait Pembangunan Gedung Baru</a>
								<time class="arial">Kamis, 13 November 2017 − 18:28 WIB</time>
								<p class="detail">Sesuai tugas pokok dan fungsinya (tupoksi), Sekretariat Jenderal DPR RI memiliki tugas mendukung kegiatan Dewan dari sisi administratif dan keahlian.</p>
							</div>
						</div>
					</div>
					<div class="list-news clearfix">
						<div class="col-md-4">
							<a href="#"><img src="{{ URL::asset('images/news-2.jpg') }}" class="lazy img-responsive" alt=""></a>
						</div>
						<div class="col-md-8">
							<div class="field-news">
								<a href="#" class="caption">Jual BUMN Harus Seizin DPR</a>
								<time class="arial">Kamis, 13 November 2017 − 18:28 WIB</time>
								<p class="detail">Anggota Komisi VI DPR RI Bambang Haryo Soekartono menyayangkan rencana Menteri Koordinator Bidang Kemaritiman Luhut Pandjaitan yang mengusulkan ke Presiden Joko Widodo untuk menjual beberapa BUMN. </p>
							</div>
						</div>
					</div>
					<div class="list-news clearfix">
						<div class="col-md-4">
							<a href="#"><img src="{{ URL::asset('images/news-3.jpg') }}" class="lazy img-responsive" alt=""></a>
						</div>
						<div class="col-md-8">
							<div class="field-news">
								<a href="#" class="caption">Komisi III DPR Tetapkan 7 Calon Komisioner Komnas HAM</a>
								<time class="arial">Kamis, 13 November 2017 − 18:28 WIB</time>
								<p class="detail">Komisi III DPR RI telah memilih tujuh calon Anggota Komisioner Komisi Nasional Hak Asasi Manusia (Komnas HAM) dari 14 calon.</p>
							</div>
						</div>
					</div>
					<div class="list-news clearfix">
						<div class="col-md-4">
							<a href="#"><img src="{{ URL::asset('images/news-4.jpg') }}" class="lazy img-responsive" alt=""></a>
						</div>
						<div class="col-md-8">
							<div class="field-news">
								<a href="#" class="caption">Keterlibatan TNI Akan Diatur Dalam UU Pencegahan Teroris</a>
								<time class="arial">Kamis, 13 November 2017 − 18:28 WIB</time>
								<p class="detail">Terkait rencana keterlibatan TNI dalam tindak pidana teroris akan diatur dengan baik dalam RUU tentang Terorisme. Hal ini, agar ada kerja sama antara TNI dan Kepolisian.</p>
							</div>
						</div>
					</div>
					<div class="list-news clearfix">
						<div class="col-md-4">
							<a href="#"><img src="{{ URL::asset('images/news-5.jpg') }}" class="lazy img-responsive" alt=""></a>
						</div>
						<div class="col-md-8">
							<div class="field-news">
								<a href="#" class="caption">Anton Sukartono Gantikan Michael Wattimena Pimpin Komisi V</a>
								<time class="arial">Kamis, 13 November 2017 − 18:28 WIB</time>
								<p class="detail">Anton Sukartono Suratto resmi dilantik menjadi Wakil Ketua Komisi V DPR RI menggantikan rekannya yang juga dari Fraksi Demokrat Michael Wattimena.</p>
							</div>
						</div>
					</div>
					<div class="list-news clearfix">
						<div class="col-md-4">
							<a href="#"><img src="{{ URL::asset('images/news-6.jpg') }}" class="lazy img-responsive" alt=""></a>
						</div>
						<div class="col-md-8">
							<div class="field-news">
								<a href="#" class="caption">Komisi V akan Bantu Perluasan Pelabuhan Laut di Saumlaki</a>
								<time class="arial">Kamis, 13 November 2017 − 18:28 WIB</time>
								<p class="detail">Pelabuhan Laut Saumlaki di Kabupaten Maluku Tenggara Barat (MTB) yang saat ini memiliki panjang sekitar 240 meter, sudah padat.</p>
							</div>
						</div>
					</div>
					<div class="list-news clearfix">
						<div class="col-md-4">
							<a href="#"><img src="{{ URL::asset('images/news-7.jpg') }}" class="lazy img-responsive" alt=""></a>
						</div>
						<div class="col-md-8">
							<div class="field-news">
								<a href="#" class="caption">Tatanan Perdagangan Dunia Saat Ini Masih Jauh Dari Ideal</a>
								<time class="arial">Kamis, 13 November 2017 − 18:28 WIB</time>
								<p class="detail">Selama ini perdagangan lintas negara dipercaya memegang peran penting untuk memajukan pertumbuhan ekonomi dan pembangunan.</p>
							</div>
						</div>
					</div>
					<div class="list-news clearfix">
						<div class="col-md-4">
							<a href="#"><img src="{{ URL::asset('images/news-8.jpg') }}" class="lazy img-responsive" alt=""></a>
						</div>
						<div class="col-md-8">
							<div class="field-news">
								<a href="#" class="caption">Pembentukan Perda Harus Perhatikan Aspek Kebutuhan Masyarakat</a>
								<time class="arial">Kamis, 13 November 2017 − 18:28 WIB</time>
								<p class="detail">Peraturan Daerah (Perda) yang dibentuk oleh Dewan Perwakilan Rakyat Daerah, baik di Tingkat I maupun Tingkat II, diharapkan dapat memperhatikan</p>
							</div>
						</div>
					</div>
					<div class="pagination main-container">
		        		<ul>
		        			<li class="active"><a href="#">1</a></li>
		        			<li><a href="#">2</a></li>
		        			<li><a href="#">3</a></li>
		        			<li><a href="#">4</a></li>
		        			<li><a href="#">></a></li>
		        			<li><a href="#">>></a></li>
		        		</ul>
		        	</div>
				</section>
				<section class="sidebar pull-right">
					<div class="tvr">
						<div class="title-widget">TVR PARLEMEN</div>
					</div>
					<div class="video">
						<div class="title-widget">VIDEO STREAMING</div>
					</div>
					<div class="social-media">
						<div class="title-widget">SOCIAL MEDIA</div>
					</div>
				</section>
			</div>
			<section class="mosaic"><img src="{{ URL::asset('images/mosaic.png') }}" alt=""></section>
		</div>
	</main>
	<footer>
		<div class="container clearfix">
			<div class="logo-footer pull-left">
				<a href="#"><img src="{{ URL::asset('images/logo.png') }}" alt=""></a>
			</div>
			<div class="social-footer pull-right">
				<ul>
					<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</footer>
	<script src='{{ URL::asset('js/jquery-1.10.2.min.js') }}'></script> <!-- jQuery -->
	<script>
		$(document).ready(function() {
			// Slide Toogle Mobile Navigation
			$(".navbar-toggle").click(function(){
			    $(".menu-mobile").slideToggle();
			});
		});
	</script>
</body>
</html>