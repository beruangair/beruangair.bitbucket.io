@extends('layouts.admin')

@section('title', 'Input Logo')

{{-- Meta --}}
@section('meta')
  @parent
@endsection

{{-- Icon --}}
@section('icon')
  @parent
@endsection

{{-- Meta Facebook --}}
@section('meta_facebook')
@endsection

{{-- Meta Twitter --}}
@section('meta_twitter')
@endsection

{{-- SEO Crawling --}}
@section('seo_crawling')
@endsection

{{-- DNS --}}
@section('dns')
@endsection

{{-- CSS --}}
@section('css')
  <link rel="stylesheet" href="{{ URL::asset('css/admin/dropzone.css') }}"> <!-- Dropzone -->
  @parent
@endsection

{{-- Topbar --}}
@section('topbar')
  @parent
@endsection

{{-- Leftbar --}}
@section('leftbar')
  @parent
@endsection

{{-- Content --}}
@section('content')
  <div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Social Media</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Daskboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Header</a></li>
                    <li class="breadcrumb-item active">Social Media</li>
                </ol>

            </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card-box table-responsive">
              <div class="row">
                <div class="col-sm-12">
                  {{ Form::open(array('url' => 'insertLogo','method' => 'post', 'enctype' => 'multipart/form-data')) }}
                  <div class="form-group">
                    <label>Logo</label>
                    <div class="input-group">
                      <div class="wrap">
                        <div class="thumb"> 
                          <img id="img" src="{{ URL::asset('images/image_upload.png') }}"/>
                        </div>
                        <div class="fileupload btn btn-purple waves-effect waves-light">
                          <span><i class="ion-upload m-r-5"></i>Upload</span>
                          <input type="file" id="upload" class="upload" name="logo_image">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group text-right m-b-0">
                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                        Submit
                    </button>
                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                        Cancel
                    </button>
                  </div>
                  {{ Form::close() }}
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end row -->
      </div> <!-- container -->
    </div> <!-- content -->

    <footer class="footer text-right">
        &copy; 2016 - 2017. All rights reserved.
    </footer>
  </div>
@endsection

{{-- Javascript --}}
@section('javascript')
  @parent
  <script type="text/javascript">
    /* Upload Preview */
    function preview(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) { $('#img').attr('src', e.target.result);  }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#upload").change(function(){
      $("#img").css({top: 0, left: 0});
        preview(this);
        $("#img").draggable({ containment: 'parent',scroll: false });
    });
  </script>
@endsection