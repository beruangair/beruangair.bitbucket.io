<?php $activePage = $this->uri->segment(2); ?>
    <div class="left side-menu">
      <div class="slimScrollDiv">
        <div class="sidebar-inner slimscrollleft">
          <!--- Divider -->
          <div id="sidebar-menu">
            <ul>
              <li class="text-muted menu-title">Navigation</li>
              <li class="has_sub <?= ($activePage == 'dashboard') ? 'active':''; ?>">
                <a href="<?php echo site_url('fungsi/dashboard') ?>" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>
              </li>
              <li class="has_sub <?= ($activePage == 'slider') ? 'active':''; ?>">
                <a href="<?php echo site_url('fungsi/slider') ?>" class="waves-effect <?= ($activePage == 'slider') ? 'active':''; ?>"><i class="ti-image"></i> <span> Slider </span></a>
              </li>
              <li class="has_sub <?= ($activePage == 'main_news' || $activePage == 'news' || $activePage == 'popular') ? 'active':''; ?>">
                <a href="#" class="waves-effect"><i class="ti-paint-bucket"></i> <span>Portal Berita</span> <span class="menu-arrow"></span> </a>
                <ul class="list-unstyled" style="">
                  <li class="<?= ($activePage == 'main_news') ? 'active':''; ?>"><a href="<?php echo site_url('fungsi/main_news') ?>">Main News</a></li>
                  <li <?= ($activePage == 'news') ? 'active':''; ?>><a href="<?php echo site_url('fungsi/news') ?>">News</a></li>
                  <li <?= ($activePage == 'popular') ? 'active':''; ?>><a href="<?php echo site_url('fungsi/popular') ?>">Popular</a></li>
                </ul>
              </li>
              <li class="text-muted menu-title">More</li>
              <li class="has_sub <?= ($activePage == 'ads') ? 'active':''; ?>">
                <a href="<?php echo site_url('fungsi/ads') ?>" class="waves-effect"><i class="ti-pencil-alt"></i> <span>Ads</span></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>