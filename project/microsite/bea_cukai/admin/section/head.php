<head>
  <title>CMS | JFA</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="CMS JFA">
  <meta name="author" content="Arif Lutfhansah">
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/icon.png">
  <!-- Mandatory Files -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/bootstrap.css"> <!-- Bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/core.css"> <!-- Core -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/components.css"> <!-- Components -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/icons.css"> <!-- Icons -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/pages.css"> <!-- Pages -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/responsive.css"> <!-- Responsive -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.css"> <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/summernote.css"> <!-- Summernote -->
  <!-- Styles Files -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/bootstrap-datepicker.min.css"> <!-- Datepicker -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/dataTables.bootstrap.min.css"> <!-- DataTable -->
  <script src="<?php echo base_url()?>assets/js/cms/modernizr.min.js"></script> <!-- Modernizr JS -->
  <script src="<?php echo base_url()?>assets/js/jquery-1.10.2.min.js"></script> <!-- jQuery JS -->
  <script src="<?php echo base_url()?>assets/js/cms/bootstrap.min.js"></script> <!-- Bootstrap JS -->
  <script src="<?php echo base_url()?>assets/js/cms/summernote.min.js"></script> <!-- Summernote -->
</head>