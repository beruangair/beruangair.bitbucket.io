<script>
  var resizefunc = [];
</script>
<!-- Mandatory Files -->
<script src="<?php echo base_url()?>assets/js/cms/detect.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/fastclick.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/jquery.slimscroll.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/jquery.blockUI.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/waves.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/wow.min.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/jquery.nicescroll.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/jquery.core.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/jquery.app.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/index.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/cms/jquery.form-pickers.init.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.dataTables.min.js"></script> <!-- jQuery DataTable -->
<script src="<?php echo base_url()?>assets/js/dataTables.bootstrap.min.js"></script> <!-- Bootstrap DataTable -->
<!-- Style Files -->
<script>
  $(document).ready(function() {
    // Init Data Table
    $('#example').DataTable();
    // Reload page setelah klik close atau klik di luar modal
    $('.data').on('hidden.bs.modal', function () {
     location.reload();
    })
    $('.input_data_summernote').summernote({
      height: 300,
      minHeight: null,
      maxHeight: null,
      focus: true,
      callbacks: {
        // Clear all formatting of the pasted text
        onPaste: function (e) {
          var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
          e.preventDefault();
          setTimeout( function(){
            document.execCommand( 'insertText', false, bufferText);
          }, 10 );
        }
      }
    });
  });
</script>