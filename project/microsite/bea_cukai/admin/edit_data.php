 <!-- Begin page -->
<div id="wrapper">

  <!-- ========== Content Start ========== -->
  <div class="content-page" style="margin-left: 0;">
    <!-- Start content -->
    <div class="modal-header">
      <button type="button" class="close reload-btn" data-dismiss="modal">&times;</button>
      <h4 class="m-t-0 header-title"><b>Tambah Artikel</b></h4>
      <p class="text-muted font-13">
        Slider adalah berita utama dalam bentuk visual yang mencakup <code>Berita Berbayar</code>, <code>Berita Terpopuler</code>, and <code>Berita Terbaru</code>.
      </p>
    </div>
    <div class="content">
      <div class="container">
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <form class="form-horizontal clearfix" role="form" method="POST" enctype="multipart/form-data" action="<?= base_url() ?>/fungsi/edit_news">
              <input type="hidden" name="id_news" value="<?php echo $id_news; ?>">
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-2 control-label">Judul</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" name="judul" ng-model="judul" value="<?php echo $title_news ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Ketegori</label>
                  <div class="col-md-10">
                    <select class="form-control" name="kategori" ng-model="kategori">
                      <?php for ($i=0;$i<count($data_category_news);$i++) {
                        if ($data_category_news[$i]->id_category==$category_news) {
                          $selected = "selected";
                        }
                        else {
                          $selected = "";
                        }
                        ?>
                        <option value="<?php echo $data_category_news[$i]->id_category;?>"<?=$selected?>><?php echo $data_category_news[$i]->name_category;?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label" for="example-email">Tanggal</label>
                  <div class="col-md-10">
                    <div class="input-group">
                      <input type="text" class="form-control" name="tanggal" placeholder="mm/dd/yyyy" id="datepicker-autoclose" ng-model="tanggal" value="<?php echo $date_news ?>">
                      <span class="input-group-addon bg-custom b-0 text-white"><i class="ti-calendar"></i></span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Penulis</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" name="penulis" ng-model="penulis" value="<?php echo $writter_news ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Artikel</label>
                  <div class="col-md-10">
                    <!--<textarea class="form-control" rows="5" name="artikel" ng-model="artikel"></textarea>-->
                    <textarea class="summernote edit_data_summernote" name="artikel"><?php echo $content_news ?></textarea>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-2 control-label">Gambar</label>
                  <div class="col-md-10">
                    <div class="control-group file-upload" id="file-upload1">
                      <div class="preview img-wrapper">
                        <img src="<?php echo base_url()?>/assets/images/<?php echo $img_news ?>"/>
                      </div>
                      <div class="file-upload-wrapper">
                        <input type="file" name="file" class="file-upload-native" accept="image/*" ng-model="gambar"/>
                        <input type="text" disabled placeholder="Upload Image" class="file-upload-text" value="<?php echo $img_news ?>" />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Slider</label>
                  <div class="col-md-10">
                    <select class="form-control" name="slider" ng-model="slider">
                      <?php 
                      if ($slider_news == "ya") {
                        echo "<option selected='yes' value='ya'>Ya</option>";
                        echo "<option value='no'>Tidak</option>";
                      }
                      else {
                        echo "<option value='ya'>Ya</option>";
                        echo "<option selected='yes' value='no'>Tidak</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Main News</label>
                  <div class="col-md-10">
                    <select class="form-control" name="main_news" ng-model="main_news">
                      <?php 
                      if ($main_news == "ya") {
                        echo "<option selected='yes' value='ya'>Ya</option>";
                        echo "<option value='no'>Tidak</option>";
                      }
                      else {
                        echo "<option value='ya'>Ya</option>";
                        echo "<option selected='yes' value='no'>Tidak</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <!-- Command If (Not Use) -->
                <?php if ($redirect == 'populer') {?>
                <?php
                  }
                ?>
                <!-- Command If -->
                <div class="form-group">
                  <label class="col-md-2 control-label">Populer</label>
                  <div class="col-md-10">
                    <select class="form-control" name="popular" ng-model="popular">
                      <?php 
                      if ($popular_news == "ya") {
                        echo "<option selected='yes' value='ya'>Ya</option>";
                        echo "<option value='no'>Tidak</option>";
                      }
                      else {
                        echo "<option value='ya'>Ya</option>";
                        echo "<option selected='yes' value='no'>Tidak</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label"></label>
                  <div class="col-md-10">
                    <button type="submit" class="btn btn-success waves-effect waves-light">Submit</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default reload-btn" data-dismiss="modal">Close</button>
    </div>
  </div>
  <!-- ========== Content End ========== -->
</div>
<script>
  $(document).ready(function() {
    $('.reload-btn').click(function(){
      location.reload();
    });
    $('.edit_data_summernote').summernote({
      height: 300, // set editor height
    });
    $('.edit_data_summernote').summernote('code', data_summernote);
  });
</script>