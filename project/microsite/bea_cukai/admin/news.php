<!DOCTYPE html>
<html class="no-js">
<?php $this->load->view('admin/section/head'); ?>
<body class="fixed-left widescreen" cz-shortcut-listen="true">
  <!-- Begin page -->
  <div id="wrapper">
    <!-- ========== Top Bar Start ========== -->
    <?php $this->load->view('admin/section/header'); ?>
    <!-- ========== Top Bar End ========== -->

    <!-- ========== Left Sidebar Start ========== -->
    <?php $this->load->view('admin/section/sidebar'); ?>
    <!-- ========== Left Sidebar End ========== -->
    
    <!-- ========== Content Start ========== -->
    <div class="content-page">
      <!-- Start content -->
      <div class="content">
        <div class="container">
          <!-- Page-Title -->
          <div class="row">
            <div class="col-sm-12">
              <h4 class="page-title">News</h4>
              <ol class="breadcrumb">
                <li>
                  <a href="#">Index</a>
                </li>
                <li class="active">
                  News
                </li>
              </ol>
            </div>
          </div>
          <!-- Tables --> 
          <div class="row tabel-data">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
                <div class="header-table clearfix">
                  <div class="title-table pull-left">
                    <h4 class="m-t-0 header-title"><b>Tabel News  </b></h4>
                    <p class="text-muted font-13 m-b-30">
                      News adalah kumpulan berita terupdate yang meliputi <code>Berita Berbayar</code>, <code>Berita Terpopuler</code>, and <code>Berita Terbaru</code>.
                    </p>
                  </div>
                  <div class="add-data pull-right">
                    <button id="addToTable" class="btn btn-default waves-effect waves-light" data-toggle="modal" data-target="#input-data">Add <i class="fa fa-plus"></i></button>
                  </div>
                </div>
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>Judul</th>
                          <th>Kategori</th>
                          <th>Tanggal</th>
                          <th>Penulis</th>
                          <th>Konten</th>
                          <th>Aksi</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php foreach($news as $n){ ?>
                    <tr>
                      <!-- <td><?php echo $n->id_news ?></td> -->
                      <td><div><?php echo str_replace('-',' ',$n->title_news) ?></div></td>
                      <td><div><?php echo $n->name_category ?></div></td>
                      <td><div><?php echo long_date($n->date_news) ?></div></td>
                      <td><div><?php echo $n->writter_news ?></div></td>
                      <td><div><?php echo mb_strimwidth($n->content_news, 0, 80, "...") ?></div></td>
                      <td class="actions">
                          <a href="<?php echo base_url().'fungsi/edit_news/'.str_replace(' ','-',$n->id_news).'/news'?>" class="on-default edit-row" data-toggle="modal" data-target="#edit-data"><i class="fa fa-pencil"></i></a>
                          <a href="<?php echo base_url().'fungsi/delete_news/'.str_replace(' ','-',$n->id_news)?>" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- Edit Data -->
          <div class="modal fade data" id="edit-data" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="m-t-0 header-title"><b>Edit Artikel</b></h4>
                  <p class="text-muted font-13">
                    Slider adalah berita utama dalam bentuk visual yang mencakup <code>Berita Berbayar</code>, <code>Berita Terpopuler</code>, and <code>Berita Terbaru</code>.
                  </p>
                </div>
                <div class="modal-body">
                  <form class="form-horizontal clearfix" role="form" method="POST" enctype="multipart/form-data" action="<?= base_url() ?>fungsi/edit_news">
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- Input Data -->
          <div class="modal fade data" id="input-data" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="m-t-0 header-title"><b>Tambah Artikel</b></h4>
                  <p class="text-muted font-13">
                    Slider adalah berita utama dalam bentuk visual yang mencakup <code>Berita Berbayar</code>, <code>Berita Terpopuler</code>, and <code>Berita Terbaru</code>.
                  </p>
                </div>
                <div class="modal-body">
                  <form class="form-horizontal clearfix" role="form" method="POST" enctype="multipart/form-data" action="<?= base_url() ?>/fungsi/news">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Judul</label>
                        <div class="col-md-10">
                          <input type="text" class="form-control" name="judul" ng-model="judul">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Ketegori</label>
                        <div class="col-md-10">
                          <select class="form-control" name="kategori" ng-model="kategori">
                            <?php for ($i=0;$i<count($category_news);$i++) {
                            ?>
                            <option value="<?php echo $category_news[$i]->id_category?>"><?php echo $category_news[$i]->name_category?></option>
                            <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label" for="example-email">Tanggal</label>
                        <div class="col-md-10">
                          <div class="input-group">
                            <input type="text" class="form-control" name="tanggal" placeholder="mm/dd/yyyy" id="datepicker-autoclose" ng-model="tanggal">
                            <span class="input-group-addon bg-custom b-0 text-white"><i class="ti-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Penulis</label>
                        <div class="col-md-10">
                          <input type="text" class="form-control" name="penulis" ng-model="penulis">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Artikel</label>
                        <div class="col-md-10">
                          <!--<textarea class="form-control" rows="5" name="artikel" ng-model="artikel"></textarea>-->
                          <textarea class="summernote input_data_summernote" name="artikel"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Gambar</label>
                        <div class="col-md-10">
                          <div class="control-group file-upload" id="file-upload1">
                            <div class="preview img-wrapper"></div>
                            <div class="file-upload-wrapper">
                              <input type="file" name="file" class="file-upload-native" accept="image/*" ng-model="gambar"/>
                              <input type="text" disabled placeholder="Upload Image" class="file-upload-text" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-10">
                          <button type="submit" class="btn btn-success waves-effect waves-light">Submit</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php $this->load->view('admin/section/footer'); ?>
    </div>
    <!-- ========== Content End ========== -->
  </div>
  <?php $this->load->view('admin/section/foot'); ?>
</body>
</html>