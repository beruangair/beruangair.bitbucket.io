<!DOCTYPE html>
<html class="no-js">
<head>
  <title>CMS | JFA</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="CMS JFA">
  <meta name="author" content="Arif Lutfhansah">
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/icon.png">
  <!-- Mandatory Files -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/bootstrap.css"> <!-- Bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/core.css"> <!-- Core -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/components.css"> <!-- Components -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/icons.css"> <!-- Icons -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/pages.css"> <!-- Pages -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/responsive.css"> <!-- Responsive -->
   <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.css"> <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/summernote.css"> <!-- Summernote -->
  <!-- Styles Files -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/cms/bootstrap-datepicker.min.css"> <!-- Datepicker -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"> <!-- DataTable -->
  <script src="<?php echo base_url()?>assets/js/cms/modernizr.min.js"></script> <!-- Modernizr JS -->
  <script src="<?php echo base_url()?>assets/js/jquery-1.10.2.min.js"></script> <!-- jQuery JS -->
  <script src="<?php echo base_url()?>assets/js/cms/bootstrap.min.js"></script> <!-- Bootstrap JS -->
  <script src="<?php echo base_url()?>assets/js/cms/summernote.min.js"></script> <!-- Summernote -->
</head>
<body class="fixed-left widescreen" cz-shortcut-listen="true">
  <!-- Begin page -->
  <div id="wrapper">
    <!-- ========== Top Bar Start ========== -->
    <div class="topbar">
      <!-- LOGO -->
      <div class="topbar-left">
        <div class="text-center">
          <a href="#" class="logo"><i class="ti-bolt icon-c-logo"></i><span>CMS<i class="ti-bolt"></i>JFA</span></a>
          <!-- Image Logo here -->
          <!--<a href="index.html" class="logo">-->
          <!--<i class="icon-c-logo"> <img src="assets/images/logo_sm.png" height="42"/> </i>-->
          <!--<span><img src="assets/images/logo_light.png" height="20"/></span>-->
          <!--</a>-->
        </div>
      </div>
      <!-- Button mobile view to collapse sidebar menu -->
      <div class="navbar navbar-default" role="navigation">
        <div class="container">
          <div class="">
            <div class="pull-left">
              <button class="button-menu-mobile open-left waves-effect waves-light">
                <i class="ti-menu"></i>
              </button>
              <span class="clearfix"></span>
            </div>
            <ul class="nav navbar-nav hidden-xs">
              <li><a href="#" class="waves-effect waves-light">Files</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </li>
            </ul>
            <form role="search" class="navbar-left app-search pull-left hidden-xs">
              <input type="text" placeholder="Search..." class="form-control">
              <a href="#"><i class="fa fa-search"></i></a>
            </form>
            <ul class="nav navbar-nav navbar-right pull-right">
              <li class="dropdown top-menu-item-xs">
                <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                  <i class="ti-bell"></i> <span class="badge badge-xs badge-danger">3</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-lg">
                  <li class="notifi-title"><span class="label label-default pull-right">New 3</span>Notification</li>
                  <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 230px;">
                    <li class="list-group slimscroll-noti notification-list" style="overflow: hidden; width: auto; height: 230px;">
                      <!-- list item-->
                      <a href="javascript:void(0);" class="list-group-item">
                        <div class="media">
                          <div class="pull-left p-r-10">
                            <em class="fa fa-diamond noti-primary"></em>
                          </div>
                          <div class="media-body">
                            <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                            <p class="m-0">
                              <small>There are new settings available</small>
                            </p>
                          </div>
                        </div>
                      </a>
                      <!-- list item-->
                      <a href="javascript:void(0);" class="list-group-item">
                        <div class="media">
                          <div class="pull-left p-r-10">
                            <em class="fa fa-cog noti-warning"></em>
                          </div>
                          <div class="media-body">
                            <h5 class="media-heading">New settings</h5>
                            <p class="m-0">
                              <small>There are new settings available</small>
                            </p>
                          </div>
                        </div>
                      </a>
                      <!-- list item-->
                      <a href="javascript:void(0);" class="list-group-item">
                        <div class="media">
                          <div class="pull-left p-r-10">
                            <em class="fa fa-bell-o noti-custom"></em>
                          </div>
                          <div class="media-body">
                            <h5 class="media-heading">Updates</h5>
                            <p class="m-0">
                              <small>There are <span class="text-primary font-600">2</span> new updates available</small>
                            </p>
                          </div>
                        </div>
                      </a>
                      <!-- list item-->
                      <a href="javascript:void(0);" class="list-group-item">
                        <div class="media">
                          <div class="pull-left p-r-10">
                            <em class="fa fa-user-plus noti-pink"></em>
                          </div>
                          <div class="media-body">
                            <h5 class="media-heading">New user registered</h5>
                            <p class="m-0">
                              <small>You have 10 unread messages</small>
                            </p>
                          </div>
                        </div>
                      </a>
                      <!-- list item-->
                      <a href="javascript:void(0);" class="list-group-item">
                        <div class="media">
                          <div class="pull-left p-r-10">
                            <em class="fa fa-diamond noti-primary"></em>
                          </div>
                          <div class="media-body">
                            <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                            <p class="m-0">
                              <small>There are new settings available</small>
                            </p>
                          </div>
                        </div>
                      </a>
                      <!-- list item-->
                      <a href="javascript:void(0);" class="list-group-item">
                        <div class="media">
                          <div class="pull-left p-r-10">
                            <em class="fa fa-cog noti-warning"></em>
                          </div>
                          <div class="media-body">
                            <h5 class="media-heading">New settings</h5>
                            <p class="m-0">
                              <small>There are new settings available</small>
                            </p>
                          </div>
                        </div>
                      </a>
                    </li>
                    <div class="slimScrollBar" style="background: rgb(152, 166, 173); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                    <div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                  </div>
                  <li>
                    <a href="javascript:void(0);" class="list-group-item text-right">
                      <small class="font-600">See all notifications</small>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="hidden-xs">
                <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="ti-new-window"></i></a>
              </li>
              <li class="dropdown top-menu-item-xs">
                <a href="#" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="/jfa/assets/images/avatar-1.jpg" alt="user-img" class="img-circle"> </a>
                <ul class="dropdown-menu">
                  <li><a href="#"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                  <li><a href="#"><i class="ti-settings m-r-10 text-custom"></i> Settings</a></li>
                  <li><a href="#"><i class="ti-lock m-r-10 text-custom"></i> Lock screen</a></li>
                  <li class="divider"></li>
                  <li><a href="#"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <!--/.nav-collapse -->
        </div>
      </div>
    </div>
    <!-- ========== Top Bar End ========== -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
      <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 766px;">
        <div class="sidebar-inner slimscrollleft" style="overflow: hidden; width: auto; height: 766px;">
          <!--- Divider -->
          <div id="sidebar-menu">
            <ul>
              <li class="text-muted menu-title">Navigation</li>
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>
              </li>
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect active"><i class="ti-paint-bucket"></i> <span>Portal Berita</span> <span class="menu-arrow"></span> </a>
                <ul class="list-unstyled" style="">
                  <li><a href="http://coderthemes.com/ubold_2.1/light/ui-notification.html">Main News</a></li>
                  <li class="active"><a href="http://coderthemes.com/ubold_2.1/light/ui-carousel.html">News</a></li>
                </ul>
              </li>
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-light-bulb"></i><span class="label label-primary pull-right">9</span><span>Anime</span> </a>
                <ul class="list-unstyled" style="">
                  <li><a href="http://coderthemes.com/ubold_2.1/light/components-grid.html">Grid</a></li>
                  <li><a href="http://coderthemes.com/ubold_2.1/light/components-widgets.html">Widgets</a></li>
                </ul>
              </li>
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-spray"></i> <span>Manga</span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled" style="">
                  <li><a href="http://coderthemes.com/ubold_2.1/light/icons-glyphicons.html">Glyphicons</a></li>
                  <li><a href="http://coderthemes.com/ubold_2.1/light/icons-materialdesign.html">Material Design</a></li>
                </ul>
              </li>
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i> <span>Ads</span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled" style="display: block;">
                  <li><a href="http://coderthemes.com/ubold_2.1/light/form-xeditable.html">X-editable</a></li>
                  <li><a href="http://coderthemes.com/ubold_2.1/light/form-image-crop.html">Image Crop</a></li>
                </ul>
              </li>
              <li class="text-muted menu-title">More</li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="slimScrollBar" style="background: rgb(152, 166, 173); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 420.614px; visibility: visible;"></div>
        <div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
      </div>
    </div>
    <!-- ========== Left Sidebar End ========== -->
    <!-- ========== Content Start ========== -->
    <div class="content-page">
      <!-- Start content -->
      <div class="content">
        <div class="container">
          <!-- Page-Title -->
          <div class="row">
            <div class="col-sm-12">
              <h4 class="page-title">Dashboard</h4>
              <ol class="breadcrumb">
                <li>
                  <a href="#">Index</a>
                </li>
                <li class="active">
                  Dashboard
                </li>
              </ol>
            </div>
          </div>
          <!-- Tables --> 
          <div class="row tabel-data">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
                <div class="header-table clearfix">
                  <div class="title-table pull-left">
                    <h4 class="m-t-0 header-title"><b>Tabel News  </b></h4>
                    <p class="text-muted font-13 m-b-30">
                      News adalah kumpulan berita terupdate yang meliputi <code>Berita Berbayar</code>, <code>Berita Terpopuler</code>, and <code>Berita Terbaru</code>.
                    </p>
                  </div>
                  <div class="add-data pull-right">
                    <button id="addToTable" class="btn btn-default waves-effect waves-light" data-toggle="modal" data-target="#input-data">Add <i class="fa fa-plus"></i></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        © 2016. All rights reserved.
      </footer>
    </div>
    <!-- ========== Content End ========== -->
  </div>
  <script>
    var resizefunc = [];
  </script>
  <!-- jQuery  -->
  <script src="/jfa/assets/js/cms/detect.js"></script>
  <script src="/jfa/assets/js/cms/fastclick.js"></script>
  <script src="/jfa/assets/js/cms/jquery.slimscroll.js"></script>
  <script src="/jfa/assets/js/cms/jquery.blockUI.js"></script>
  <script src="/jfa/assets/js/cms/waves.js"></script>
  <script src="/jfa/assets/js/cms/wow.min.js"></script>
  <script src="/jfa/assets/js/cms/jquery.nicescroll.js"></script>
  <script src="/jfa/assets/js/cms/jquery.scrollTo.min.js"></script>
  <script src="/jfa/assets/js/cms/jquery.core.js"></script>
  <script src="/jfa/assets/js/cms/jquery.app.js"></script>
  <script src="/jfa/assets/js/cms/index.js"></script>
  <!-- Style Files -->
  <script src="/jfa/assets/js/cms/bootstrap-timepicker.js"></script>
  <script src="/jfa/assets/js/cms/bootstrap-colorpicker.min.js"></script>
  <script src="/jfa/assets/js/cms/bootstrap-datepicker.js"></script>
  <script src="/jfa/assets/js/cms/jquery.form-pickers.init.js"></script>
  <!-- DataTable -->
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> <!-- jQuery DataTable -->
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script> <!-- Bootstrap DataTable -->

  <script>
    $(document).ready(function() {
      // Init Data Table
      $('#example').DataTable();
      // Reload page setelah klik close atau klik di luar modal
      $('.data').on('hidden.bs.modal', function () {
       location.reload();
      })
      $('.input_data_summernote').summernote({
        height: 300, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: true // set focus to editable area after initializing summernote
      });
    });
  </script>
</body>
</html>