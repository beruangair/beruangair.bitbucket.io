<!DOCTYPE html>
<html lang="en">
<head>
	<title>SindoNews</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="description" content="SindoNews">
	<meta name="author" content="Arif Lutfhansah">
	<link href=assets/images/favicon.ico rel="shortcut icon">
	<!-- Base -->
	<link rel="stylesheet" href="assets/css/base.css">
	<!-- Haeader -->
	<link rel="stylesheet" href="assets/css/header.css">
	<!-- Style -->
	<link rel="stylesheet" href="assets/css/styles.css">
	<!-- BX Slider -->
	<link rel="stylesheet" href="assets/css/jquery.bxslider.css">
	<!-- Footer -->
	<link rel="stylesheet" href="assets/css/footer.css">
</head>
<body>
	<header>
		<div class="brand">
			<div class="container clearfix">
				<div class="logo pull-left">
					<a href="#"><img src="assets/images/logo.png" alt="Kabupaten lahat Sumatera Selatan"/></a>
				</div>
				<div class="sosmed pull-right">
					<ul>
						<li><a href="#"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus fa-lg" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>
	<main>
		<!-- Slider -->
		<section class="slider">
			<ul class="bxslider">
				<li>
					<img src="assets/images/slider_01.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"/>
				</li>
				<li>
					<img src="assets/images/slider_01.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"/>
				</li>
				<li>
					<img src="assets/images/slider_01.jpg" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"/>
				</li>
			</ul>
		</section>
		<section class="article content clearfix">
			<div class="container clearfix">
				<div class="col-md-4">
					<div class="inner">
						<div class="box">
							<div class="ci"><a href="#"><img src="assets/images/content_001.jpg" alt="Content 1"/></a></div>
							<div class="ct"><a href="#">Bea Cukai Bekasi Musnahkan Seribu Botol Minuman Beralkohol</a></div>
							<div class="cc"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p></div>
							<a href="#" class="btn-more">Read More</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="inner">
						<div class="box">
							<div class="ci"><a href="#"><img src="assets/images/content_002.jpg" alt="Content 1"/></a></div>
							<div class="ct"><a href="#">Bea Cukai Sosialisasikan Desain Pita Cukai Tahun 2017</a></div>
							<div class="cc"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p></div>
							<a href="#" class="btn-more">Read More</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="inner">
						<div class="box">
							<div class="ci"><a href="#"><img src="assets/images/content_003.jpg" alt="Content 1"/></a></div>
							<div class="ct"><a href="#">Bea Cukai Beberkan Kronologi Pemasukan KTP dan NPWP di Soekarno-Hatta</a></div>
							<div class="cc"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p></div>
							<a href="#" class="btn-more">Read More</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="inner">
						<div class="box">
							<div class="ci"><a href="#"><img src="assets/images/content_004.jpg" alt="Content 1"/></a></div>
							<div class="ct"><a href="#">AEO Indonesia, Jaminan Keamanan Rantai Pasokan Logistik</a></div>
							<div class="cc"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p></div>
							<a href="#" class="btn-more">Read More</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="inner">
						<div class="box">
							<div class="ci"><a href="#"><img src="assets/images/content_005.jpg" alt="Content 1"/></a></div>
							<div class="ct"><a href="#">Bea Cukai dan BNN Tumpas Narkotika di Perbatasan Kalimantan</a></div>
							<div class="cc"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p></div>
							<a href="#" class="btn-more">Read More</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="inner">
						<div class="box">
							<div class="ci"><a href="#"><img src="assets/images/content_006.jpg" alt="Content 1"/></a></div>
							<div class="ct"><a href="#">Peringatan HPI 2017 Data Analysis For Effective Border Management</a></div>
							<div class="cc"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p></div>
							<a href="#" class="btn-more">Read More</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="pagination container">
			<ul>
				<li class="active"><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">></a></li>
				<li><a href="#">>></a></li>
			</ul>
		</section>
		<section class="r_center">
			<img src="assets/images/r_center.jpg" class="img-responsive" alt=""/>
		</section>
		<section class="video">
			<div class="container">
				<h3>BC Video Streaming</h3>
				<iframe src="https://www.youtube.com/embed/Io2QljVRArQ?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</section>
		<section class="radio">
			<div class="container">
				<h3 style="color: #fff;">Radio Section</h3>
			</div>
		</section>
		<section class="images">
			<ul class="image-slider">
			  <li><img src="assets/images/image_01.jpg" /></li>
			  <li><img src="assets/images/image_02.jpg" /></li>
			  <li><img src="assets/images/image_03.jpg" /></li>
			  <li><img src="assets/images/image_04.jpg" /></li>
			</ul>
		</section>
	</main>
	<footer>
		<div class="copyright">
			<div class="container">
				<p>Copyright © 2017 SINDOnews.com All right reserved</p>
			</div>
		</div>
	</footer>
	<script src='assets/js/jquery-1.10.2.min.js'></script> <!-- jQuery -->
	<script src='assets/js/jquery.bxslider.js'></script> <!-- BX Slider JS -->
	<script>
		$('.bxslider').bxSlider({
			auto: true,
			captions: true
		});
		$('.image-slider').bxSlider({
			pager: false,
			controls: true,
			minSlides: 3,
		  maxSlides: 4,
		  slideWidth: 265,
		  slideMargin: 20
	  });
	</script>
</body>
</html>