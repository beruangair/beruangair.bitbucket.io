/*!
* 									portalPromo.js
*									--------------
* 					A promotions and tutorial popup jQuery plugin
*
* 			Original author: 			    David Birchall: davidHB.com
* 			Originally developed for: 		MoneyPlus Group: moneyplus.com
* 			Further changes, comments:  	dhbirchall@me.com
*
*			Requires: jQuery.js, portalPromo.css, font-awesome.css, animate.css
*
* 							Licensed under the MIT license
*/

// the semi-colon before protects against scripts not closed properly.
;(function ( $, window, document, undefined ) {
// in ES3, "undefined" is modifiable so pass it as a var in our scope.
// window and document passed in to make smaller file when minified.

	// Create the defaults
    var pluginName = 'portalPromo',
        defaults = {
        	contentType: "tutorial",
          allowTutorial: true,
            	 adPage: 0
        };

    // The actual plugin constructor
    function Plugin( element, options ) {
        this.element = element;
        // merges contents of two or more objects stores result in the first object.
        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype.init = function () {

	    		var self = this;// give "this" global scope

				var	   tutPage = 0,	// the advert to disply initially
				   adContainer = '.advert', // the container of the advertisement divs
				    	advert = '.ad-page', // the divs containing the advertisements
				  tutContainer = '.tutorial', // the container of the advertisement divs
				      tutorial = '.tut-page', // the divs containing the tutorial pages
				      backdrop = '<div class="darken perfect-center animated fadeIn"></div>', // markup for backdrop
				         popup = "<div class='portalPromo perfect-center animated zoomInDown'>" +
								  "<div class='close-btn'>" +
									    "<i class='fa fa-close'></i>" +
								  "</div>" +
								  "<div class='content-div'>" +
								  "</div>" +
								  "<div class='bottom-nav'>" +
									    "<div class='allow-tut'>" +
									      "<input type='checkbox'>" +
									      "<span>Don't Show Again</span>" +
									  	"</div>" +
									    "<div class='next'>" +
									      "<span>Next</span>" +
									        "<i class='fa fa-chevron-right'></i>" +
									    "</div>" +
									     "<div class='previous'>" +
									         "<i class='fa fa-chevron-left'></i>" +
									         "<span>Previous</span>" +
									    "</div>" +
								  "</div>" +
							 "</div>"; // markup for modal

				   self.open = function(){

				   	$(self.element).append( backdrop );

				   	$(self.element).append( popup );

				   	 self.loadContent();

				   	 self.closeOnClick();

				   }


				self.closeModal = function(){

					$('<div class="darken perfect-center animated fadeIn"></div>');
				        
				        $('.portalPromo')
				        	.removeClass('animated zoomInDown')
				      	  	.addClass('animated zoomOut');

				}

			   self.closeOnClick = function(){

					$('.portalPromo .close-btn').click(function(){ 
	    
				   		self.closeModal();

					});

				}

				self.loadContent = function(){

					if (self.options.contentType === "tutorial"){

				        self.navigateOnClick();

				        $('.portalPromo .bottom-nav').removeClass('hidden');

				        $('.portalPromo .content-div').html( $( tutorial ).eq( tutPage ).clone() );

				      } else if ( self.options.contentType === "advert" ){

				      	var adPage;

				      	if ( self.options.adPage === "random" || typeof self.options.adPage !== "string" ){

				      		adPage = Math.floor( Math.random() * $(advert).length );

				      	} else {

				      		adPage = self.options.adPage;

				      	}

				        $('.portalPromo .bottom-nav').addClass('hidden');

				        $('.portalPromo .content-div').html( $(advert).eq( adPage ).clone() );

				    }

				}

				self.navigateOnClick = function(){

				      $('.portalPromo .previous').addClass('hidden');

				      function nextPage(){
				      	// set next page variable
				        tutPage += 1;

				        // if the page (zero indexed) is LESS than length (which starts from one)
				        if ( tutPage < ( $( tutContainer + " " + tutorial ).length ) ){

				        	// put the new page in
				        	$('.portalPromo .content-div').html( $(tutorial).eq(tutPage).clone() );

				        }

				        // add the previous button back
				        $('.portalPromo .previous').removeClass('hidden');

				        //  e.g: the length is 5 pages so tutPage = 0,1,2,3,4
				        // so when tutPage reaches 4 which is 1 minus the length... 
				        if ( tutPage >= ( $( tutContainer + " " + tutorial ).length -1 ) ){

						   // turn it into a finish button
				          $('.portalPromo .next')
				          	.html('<span class="finish">Finish</span><i class="fa fa-check-square-o finish"></i>');

				           // e.g: when tutPage reaches 5 which is equal to the length,
				           if ( tutPage >= ( $( tutContainer + " " + tutorial ).length ) ){

					           // it means they clicked again, this time on the finish button
					           // so close the modal down and change the variable back to the correct page
					           	self.closeModal();
					           	tutPage = ( $( tutContainer + " " + tutorial ).length -1 );
				            }

				        }

				      };// end next page

				      function previousPage(){

				          tutPage -= 1;

				          if (tutPage <= 0){

				          	tutPage = 0;

				            $('.portalPromo .previous').addClass('hidden');

				          }
				          // if previous is clicked and we're on the last page
				          if ( tutPage < $( tutContainer + " " + tutorial ).length ){

				          	// change it back to a next button
				            $('.portalPromo .next')
				            	.html('<span>Next</span><i class="fa fa-chevron-right"></i>');

				          }
				        
				          $('.portalPromo .content-div').html( $(tutorial).eq(tutPage).clone() );

				    };// end previous page

				    $('.portalPromo .previous').click(function(){
				    	previousPage();
				    });

				    $('.portalPromo .next').click(function(){
				    	nextPage();
				    });

				}// end navigate on click

		if ( self.options.allowTutorial === true || self.options.contentType === "advert"){

			self.open();

		}

    };

    // plugin wrapper around the constructor, prevent against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName,
                new Plugin( this, options ));
            }
        });
    }

})( jQuery, window, document );



		//	Initialise the plugin like this on the page you need it
		//	(the element to apply the plugin has been tested only on the body element of your page.)
		$('body').portalPromo({
							  allowTutorial: true,// if this is false and contentType is "advert"
								contentType:"tutorial",// accepts "tutorial" or "advert"
									 adPage:"random"// accepts "random" or zero based index
							  });