$(document).ready(function()
{  
  var $pen = $('.pen');
  var $clicker = $('.clicker');
  var $point = $('.point');
  var $logo = $('.spinner img');
  
  $pen.click(function()
  {
    if ($pen.hasClass('active')) {
      $clicker.animate({width: '10px'}, '100')
        .animate({width: '30px'}, '100');
    
      $point.animate({width: '15px'}, '100')
        .animate({width: '0px'}, '100');
      
      $logo
        .animate({left:'-=15px'}, '100')
        .animate({bottom:'-=40px'}, '20',
          function(){
            $(this).css({bottom:'22px', left:'0'});
          });
      $(this).removeClass('active');
    }
    else {
      $clicker.animate({width: '10px'}, '100')
        .animate({width: '15px'}, '100');
    
      $point.animate({width: '15px'}, '100')
        .animate({width: '10px'}, '100');
    
      $logo.animate({bottom: '-=40px'}, '120');
      
      $(this).addClass('active');
    }
  });
});