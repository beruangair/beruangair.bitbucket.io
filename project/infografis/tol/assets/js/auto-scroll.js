$(document).ready(function() {
	/* Start Scroll */
	var oneSecond = 6000;
	$('.smoothScroll').on('click', function() {
	    $("html, body").animate({ 
	    	scrollTop: $(document).height() 
	    }, 30 * oneSecond);
	    $("html, body").bind("mousewheel", function() {
		    return false;
		});
	    return false;
	});
	/* Stop Scroll */
	$('.stopscroll').on('click', function() {
		$("html, body").bind("scroll mousedown DOMMouseScroll mousewheel keyup", function(){
		    $('html, body').stop();
		});
		$("html, body").bind("mousewheel", function() {
		    return true;
		});
	});
});