$(document).ready(function() {
  /* --- Membuat Efek Animasi --- */
  new WOW().init();

  /* --- Membuat Efek Fade In Ketika Scroll --- */
  $(window).scroll(function() {
    var width = screen.width;
    console.log($(window).scrollTop())
    /* --- Width > 1440px (HDPI) --- */
    if (width > 1440){
      if ($(this).scrollTop() > 460){
        $('.box.one').addClass('fadein');
      }
      if ($(this).scrollTop() > 2300){
        $('.box.two').addClass('fadein');
      }
      if ($(this).scrollTop() > 6450){
        $('.box.three').addClass('fadein');
      }
      if ($(this).scrollTop() > 8300){
        $('.box.four').addClass('fadein');
      }
      if ($(this).scrollTop() > 12450){
        $('.box.five').addClass('fadein');
      }
      if ($(this).scrollTop() > 14300){
        $('.box.six').addClass('fadein');
      }
      if ($(this).scrollTop() > 18450){
        $('.box.seven').addClass('fadein');
      }
      if ($(this).scrollTop() > 20300){
        $('.box.eight').addClass('fadein');
      }
      if ($(this).scrollTop() > 24450){
        $('.box.nine').addClass('fadein');
      }
      if ($(this).scrollTop() > 26300){
        $('.box.ten').addClass('fadein');
      }
      if ($(this).scrollTop() > 28450){
        $('.box.eleven').addClass('fadein');
      }
      if ($(this).scrollTop() > 30300){
        $('.box.twelve').addClass('fadein');
      }
      if ($(this).scrollTop() > 34450){
        $('.box.thirteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 36300){
        $('.box.fourteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 40450){
        $('.box.fifteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 42300){
        $('.box.sixteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 46450){
        $('.box.seventeen').addClass('fadein');
      }
      if ($(this).scrollTop() > 48300){
        $('.box.eightteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 52450){
        $('.box.nineteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 54300){
        $('.box.twenty').addClass('fadein');
      }
      if ($(this).scrollTop() > 58450){
        $('.box.twentyone').addClass('fadein');
      }
      if ($(this).scrollTop() > 60300){
        $('.box.twentytwo').addClass('fadein');
      }
      if ($(this).scrollTop() > 67000){
      }
      else{  
      }
    }
    /* --- Width > 1280px (MDPI) --- */
    if ((width > 1279)){
      if ($(this).scrollTop() > 1000){
        $('.box.one').addClass('fadein');
      }
      if ($(this).scrollTop() > 2500){
        $('.box.two').addClass('fadein');
      }
      if ($(this).scrollTop() > 5500){
        $('.box.three').addClass('fadein');
      }
      if ($(this).scrollTop() > 7000){
        $('.box.four').addClass('fadein');
      }
      if ($(this).scrollTop() > 10000){
        $('.box.five').addClass('fadein');
      }
      if ($(this).scrollTop() > 11500){
        $('.box.six').addClass('fadein');
      }
      if ($(this).scrollTop() > 14500){
        $('.box.seven').addClass('fadein');
      }
      if ($(this).scrollTop() > 16000){
        $('.box.eight').addClass('fadein');
      }
      if ($(this).scrollTop() > 19000){
        $('.box.nine').addClass('fadein');
      }
      if ($(this).scrollTop() > 20500){
        $('.box.ten').addClass('fadein');
      }
      if ($(this).scrollTop() > 23500){
        $('.box.eleven').addClass('fadein');
      }
      if ($(this).scrollTop() > 25000){
        $('.box.twelve').addClass('fadein');
      }
      if ($(this).scrollTop() > 28000){
        $('.box.thirteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 29500){
        $('.box.fourteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 32500){
        $('.box.fifteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 34000){
        $('.box.sixteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 37000){
        $('.box.seventeen').addClass('fadein');
      }
      if ($(this).scrollTop() > 38500){
        $('.box.eightteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 41500){
        $('.box.nineteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 43000){
        $('.box.twenty').addClass('fadein');
      }
      if ($(this).scrollTop() > 46000){
        $('.box.twentyone').addClass('fadein');
      }
      if ($(this).scrollTop() > 47500){
        $('.box.twentytwo').addClass('fadein');
      }
      else{      
      }
    }
    /* --- Width > 768px (SDPI) --- */
    if ((width >= 768 && width <= 1024)){
      if ($(this).scrollTop() > 460){
        $('.box.one').addClass('fadein');
      }
      if ($(this).scrollTop() > 1550){
        $('.box.two').addClass('fadein');
      }
      if ($(this).scrollTop() > 3450){
        $('.box.three').addClass('fadein');
      }
      if ($(this).scrollTop() > 4450){
        $('.box.four').addClass('fadein');
      }
      if ($(this).scrollTop() > 6350){
        $('.box.five').addClass('fadein');
      }
      if ($(this).scrollTop() > 7350){
        $('.box.six').addClass('fadein');
      }
      if ($(this).scrollTop() > 9250){
        $('.box.seven').addClass('fadein');
      }
      if ($(this).scrollTop() > 10250){
        $('.box.eight').addClass('fadein');
      }
      if ($(this).scrollTop() > 12150){
        $('.box.nine').addClass('fadein');
      }
      if ($(this).scrollTop() > 13150){
        $('.box.ten').addClass('fadein');
      }
      if ($(this).scrollTop() > 15050){
        $('.box.eleven').addClass('fadein');
      }
      if ($(this).scrollTop() > 16050){
        $('.box.twelve').addClass('fadein');
      }
      if ($(this).scrollTop() > 17950){
        $('.box.thirteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 18950){
        $('.box.fourteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 20850){
        $('.box.fifteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 21850){
        $('.box.sixteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 23750){
        $('.box.seventeen').addClass('fadein');
      }
      if ($(this).scrollTop() > 24750){
        $('.box.eightteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 26650){
        $('.box.nineteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 27650){
        $('.box.twenty').addClass('fadein');
      }
      if ($(this).scrollTop() > 29550){
        $('.box.twentyone').addClass('fadein');
      }
      if ($(this).scrollTop() > 29550){
        $('.box.twentytwo').addClass('fadein');
      }
      else{    
      }
    }
    /* --- Width < 768px (Mobile) --- */
    if (width <= 768){
      $('.box').addClass('fadein');
    }
  });
  /* --- Membuat Graph Chart --- */
  $('td').each(function() {
    var max = 5000;
    var cell = $(this).data('graph');
    var cellHeight = (cell / max) * 100;
    $('span', this).height(cellHeight + '%');
  });
  /* --- back To Top When Reload --- */
  $(this).scrollTop(0);
  /* --- Menghilangkan Floating Button --- */
  $(".close-floating").click(function(){
    $(".float-button").css({"right": "-150px"});
  });
  $(".share-sosmed").click(function(){
    $(".float-button").css({"right": "-150px"});
    $(".float-sosmed").css({"right": "0"});
  });
  $(".back-floating").click(function(){
    $(".float-button").css({"right": "0"});
    $(".float-sosmed").css({"right": "-150px"});
  });
  /* --- Menampilkan Floating Button Ketika Top Windows --- */
  $(window).scroll(function() {
    if ($(this).scrollTop() >= 4933) {
      $(".float-button").css({"right": "0"});
    }
  });
});