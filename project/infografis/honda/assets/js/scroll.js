//set body height to the width of the horizontal scroll area
//$('body').css('height', $('.horizontal-scroll-area')[0].clientWidth * $('.item').length + 'px'); DEFAULT
$('body').css({'height': '71040px'});
//translate vertical scrolling into horizontal scrolling
var prevTop = 0;
$(window).on('scroll', function(){
  //get current vertical scroll value
  var currentTop = $(this).scrollTop();
  //find out how much user has scrolled since last time the event was fired
  var amountScrolled = currentTop - prevTop;
  //add this value to the current horizontal scroll area value
  var horizontalScrollArea = $('.horizontal-scroll-area');
  //set new scroll value
  $('.horizontal-scroll-area').scrollLeft($('.horizontal-scroll-area').scrollLeft() +  amountScrolled);
  //update previous scroll
  prevTop = currentTop;
})