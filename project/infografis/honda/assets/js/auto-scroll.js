$(document).ready(function() {
	/* Start Scroll */
	var oneSecond = 20000;
	$('.smoothScroll').on('click', function() {
	    $("html, body").animate({ 
	    	scrollTop: $(document).height() 
	    }, 30 * oneSecond, 'linear');
	    $("html, body").bind("mousewheel", function() {
		    return false;
		});
		$('.stopscroll').on('click', function() {
			$("html, body").animate({ 
		    	scrollTop: $(document).height() 
		    }, 0 * oneSecond, 'linear');
		});
	    return false;
	});
	/* Stop Scroll */
});