$(document).ready(function() {
  /* --- Membuat Efek Animasi --- */
  new WOW().init();

  /* --- Membuat Efek Fade In Ketika Scroll --- */
  $(window).scroll(function() {
    var width = screen.width;
    console.log($(window).scrollTop())
    /* --- Width > 1440px (HDPI) --- */
    if (width > 1440){
      var i;
      var number = ["one", "two", "three", "four", "five", "six", "seven", "eight"];
      var scroll = ["460", "2300", "6450", "8300", "12450", "14300", "18450", "20300"];
      for (i = 0; i < 8; i++) { 
          if ($(this).scrollTop() > scroll[i]){
            $('.box.'+ number[i]).addClass('fadein');
          } else {  
          }
      }
      /*if ($(this).scrollTop() > 460){
        $('.box.one').addClass('fadein');
      }
      if ($(this).scrollTop() > 2300){
        $('.box.two').addClass('fadein');
      }
      if ($(this).scrollTop() > 6450){
        $('.box.three').addClass('fadein');
      }
      if ($(this).scrollTop() > 8300){
        $('.box.four').addClass('fadein');
      }
      if ($(this).scrollTop() > 12450){
        $('.box.five').addClass('fadein');
      }
      if ($(this).scrollTop() > 14300){
        $('.box.six').addClass('fadein');
      }
      if ($(this).scrollTop() > 18450){
        $('.box.seven').addClass('fadein');
      }
      if ($(this).scrollTop() > 20300){
        $('.box.eight').addClass('fadein');
      }
      if ($(this).scrollTop() > 24450){
        $('.box.nine').addClass('fadein');
      }
      if ($(this).scrollTop() > 26300){
        $('.box.ten').addClass('fadein');
      }
      if ($(this).scrollTop() > 28450){
        $('.box.eleven').addClass('fadein');
      }
      if ($(this).scrollTop() > 30300){
        $('.box.twelve').addClass('fadein');
      }
      if ($(this).scrollTop() > 34450){
        $('.box.thirteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 36300){
        $('.box.fourteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 40450){
        $('.box.fifteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 42300){
        $('.box.sixteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 46450){
        $('.box.seventeen').addClass('fadein');
      }
      if ($(this).scrollTop() > 48300){
        $('.box.eightteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 52450){
        $('.box.nineteen').addClass('fadein');
      }
      if ($(this).scrollTop() > 54300){
        $('.box.twenty').addClass('fadein');
      }
      if ($(this).scrollTop() > 58450){
        $('.box.twentyone').addClass('fadein');
      }
      if ($(this).scrollTop() > 60300){
        $('.box.twentytwo').addClass('fadein');
      }*/
    }
    if ($(this).scrollTop() > 55555){
        $(".car img").css({"bottom": "-800px"});
      }
      else{
        $(".car img").css({"bottom": "20px"});
      }
      /* --- Menghilangkan Mobil Ketika Sroll --- */
      if (width <= 768){
        if ($(this).scrollTop() > 42414){
          $(".car img").css({"bottom": "-800px"});
        }
        else{
          $(".car img").css({"bottom": "20px"});
        }
      }
  });
  /* --- Membuat Graph Chart --- */
  $('td').each(function() {
    var max = 5000;
    var cell = $(this).data('graph');
    var cellHeight = (cell / max) * 100;
    $('span', this).height(cellHeight + '%');
  });
  /* --- back To Top When Reload --- */
  $(this).scrollTop(0);
  /* --- Menghilangkan Floating Button --- */
  $(".close-floating").click(function(){
    $(".float-button").css({"right": "-150px"});
  });
  $(".share-sosmed").click(function(){
    $(".float-button").css({"right": "-150px"});
    $(".float-sosmed").css({"right": "0"});
  });
  $(".back-floating").click(function(){
    $(".float-button").css({"right": "0"});
    $(".float-sosmed").css({"right": "-150px"});
  });
  /* --- Menampilkan Floating Button Ketika Top Windows --- */
  $(window).scroll(function() {
    if ($(this).scrollTop() >= 4933) {
      $(".float-button").css({"right": "0"});
    }
  });
});